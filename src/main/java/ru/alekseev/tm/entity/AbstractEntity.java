package ru.alekseev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractEntity {
    @NotNull private final String id = UUID.randomUUID().toString();
}
