package ru.alekseev.tm.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public final class User extends AbstractEntity{

    @NotNull private final String id = UUID.randomUUID().toString();
    @Nullable private String login;
    @Nullable private String passwordHashcode;
    @Nullable private RoleType roleType;
}
