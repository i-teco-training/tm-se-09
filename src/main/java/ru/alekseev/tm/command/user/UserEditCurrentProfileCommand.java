package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.RoleType;
import ru.alekseev.tm.entity.User;

public final class UserEditCurrentProfileCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "edit-profile";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Edit your Project Manager profile";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[EDITING YOUR PROFILE]");
        @NotNull final User currentUser = serviceLocator.getUserService().getCurrentUser();
        System.out.println("If you want to update account type enter \"1\" otherwise enter any letter");
        @NotNull final String accountTypeUpdateChoice = serviceLocator.getTerminalServise().getFromConsole();
        if ("1".equals(accountTypeUpdateChoice)) {
            System.out.println("Enter \"1\" to chose \"admin\" account type otherwise enter any letter");
            @NotNull final String newAccountType = serviceLocator.getTerminalServise().getFromConsole();
            if ("1".equals(newAccountType)) {
                currentUser.setRoleType(RoleType.admin);
            } else currentUser.setRoleType(RoleType.user);
        }
        serviceLocator.getUserService().update(currentUser);
        System.out.println("[YOUR PROFILE IS UPDATED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
