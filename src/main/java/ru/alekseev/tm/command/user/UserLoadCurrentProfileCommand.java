package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.User;

public final class UserLoadCurrentProfileCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "load-profile";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Load your Project Manager profile";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOADING YOUR PROFILE]");
        @NotNull final User currentUser = serviceLocator.getUserService().getCurrentUser();
        System.out.println("Your login: " + currentUser.getLogin());
        System.out.println("Your id: " + currentUser.getId());
        if (currentUser.getRoleType() != null) {
            System.out.println("Your account type: " + currentUser.getRoleType().toString());
        }
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
