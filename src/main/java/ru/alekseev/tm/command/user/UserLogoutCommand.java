package ru.alekseev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "logout";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Logout from Project Manager";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOGGING OUT]");
        serviceLocator.getUserService().setCurrentUser(null);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
