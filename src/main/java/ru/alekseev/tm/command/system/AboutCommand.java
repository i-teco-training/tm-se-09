package ru.alekseev.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;

public final class AboutCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "about";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Get app version information";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("Manifest Version is " + Manifests.read("Manifest-Version"));
        System.out.println("Сurrent build number is " + Manifests.read("AppVersion"));
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
