package ru.alekseev.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ExitCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "exit";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Stop the Project Manager Application";
    }

    @Override
    public final void execute() {
        System.exit(0);
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
