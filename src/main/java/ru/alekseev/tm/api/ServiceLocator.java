package ru.alekseev.tm.api;

import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.service.TerminalService;

import java.util.List;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    TerminalService getTerminalServise();

    List<AbstractCommand> getCommands();
}
