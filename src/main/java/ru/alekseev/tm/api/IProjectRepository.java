package ru.alekseev.tm.api;

import ru.alekseev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findAllByUserId(final String userId);

    void deleteByUserId(String userId);

    void updateByUserIdProjectIdProjectName(String userId, String projectId, String projectName);

    void deleteByUserIdAndProjectId(String userId, String projectId);
}
