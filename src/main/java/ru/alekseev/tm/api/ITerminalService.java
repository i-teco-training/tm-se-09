package ru.alekseev.tm.api;

public interface ITerminalService {

    String getFromConsole();
}
